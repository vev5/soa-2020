const users = require('./users.json');

const Koa = require('koa');
const Router = require('@koa/router');
const koaBody = require('koa-body');
const jwt = require('jsonwebtoken');
const cors = require('@koa/cors');

const port = 5001;

const router = new Router();
const secret = 'my-secret';

const app = new Koa();
app.use(cors());

app.use(koaBody());

router.post('/login', ctx => {
    ctx.status = 200;
    const { username, password } = ctx.request.body;

    const user = users.find(u => u.username === username && u.password === password);

    if (user) {
        ctx.body = { token: jwt.sign(ctx.request.body, secret), username, password };
    } else {
        ctx.status = 403;
    }
});

router.post('/verify', ctx => {
    ctx.status = 200;
    try {
        const { username, password } = jwt.verify(ctx.request.body.token, secret);
        if (users.find(u => u.username === username && u.password === password)) {
            ctx.body = { authorized: true };
        } else {
            ctx.status = 401;
            ctx.body = { authorized: false };
        }
    } catch (err) {
        ctx.status = 401;
    }
})

app.use(router.routes())
    .use(router.allowedMethods());

app.listen(port);
console.log(`Listening on port ${port}.`);