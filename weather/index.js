const Koa = require('koa');
const app = new Koa();
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const fetch = require('node-fetch');

const port = 5004;

const apiKey = 'hehe';

app.use(koaBody());

router.get('/weather', async ctx => {
    const { name } = ctx.request.query;
    if (name) {
        const authResponse = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${name}&appid=${apiKey}&units=metric`, { method: 'GET' }).then(res => res.json());
        console.log(authResponse);
        ctx.response.body = authResponse;
    } else {
        ctx.response.body = {};
    }
});

app.use(router.routes())
    .use(router.allowedMethods());

app.listen(port);

console.log(`Listening on port ${port}.`);
