const Koa = require('koa');
const app = new Koa();
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const fetch = require('node-fetch');
const logger = require('koa-logger')
const WebSocket = require('ws');
const KoaLogger = require('koa-logger');
const cors = require('@koa/cors');

const wsPort = 5050;
const port = 5000;

const wss = new WebSocket.Server({ port: wsPort })

wss.on('connection', ws => {
    ws.on('message', msg => {
        const data = JSON.parse(msg);
        if (data.username) {
            ws.username = data.username;
        }
    });
});

const notify = (data) => {
    const clients = Array.from(wss.clients);
    const connected = clients.filter(c => c.username && c.username === data.username);
    connected.forEach(client => client.send(JSON.stringify(data)));
}

const endpoints = {
    auth: {
        login: 'http://auth:5001/login',
        verify: 'http://auth:5001/verify',
    },
    locations: 'http://locations:5003/location',
    weather: 'http://weather:5004/weather'
}

app.use(cors());
app.use(koaBody());
app.use(KoaLogger());

const authorize = async (ctx, next) => {
    const authHeader = ctx.request.headers.authorization;
    if (!authHeader) {
        ctx.status = 401;
        return;
    }

    const [, token] = authHeader.split(' ');
    const authResponse = await fetch(endpoints.auth.verify, { method: 'POST', body: JSON.stringify({ token }), headers: { 'Content-Type': 'application/json' } }).then(res => res.json());

    if (authResponse.authorized) {
        await next();
    } else {
        ctx.status = 401;
    }
}

router.post('/login', async ctx => {
    const authResponse = await fetch(endpoints.auth.login, {
        method: 'POST', body: JSON.stringify(ctx.request.body), headers: { 'Content-Type': 'application/json' },
    }).then(res => res.json());
    ctx.response.body = authResponse;
});

router.post('/location', authorize, async ctx => {
    const authResponse = await fetch(endpoints.locations, {
        method: 'POST', body: JSON.stringify(ctx.request.body), headers: { 'Content-Type': 'application/json' },
    }).then(res => res.json());

    notify(authResponse);

    ctx.response.body = authResponse;
});

router.get('/location', authorize, async ctx => {
    const { username } = ctx.request.query;
    const authResponse = await fetch(`${endpoints.locations}?username=${username}`, { method: 'GET' }).then(res => res.json());
    ctx.response.body = authResponse;
});

router.get('/weather', authorize, async ctx => {
    const { name } = ctx.request.query;
    const authResponse = await fetch(`${endpoints.weather}?name=${name}`, { method: 'GET' }).then(res => res.json());
    ctx.response.body = authResponse;
});

app.use(router.routes())
    .use(router.allowedMethods());

app.listen(port);
console.log(`Listening on port ${port}.`);
