const Koa = require('koa');
const app = new Koa();
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const fetch = require('node-fetch');

const port = 5003;

const MongoClient = require('mongodb').MongoClient;
const mongoUrl = 'mongodb://mongo:27017/soa';

let db;

MongoClient.connect(mongoUrl, (err, database) => {
    db = database.db('soa');
    if (err) throw err;
    console.log('Database connection established.');
});

app.use(koaBody());

router.post('/location', async ctx => {
    const { body } = ctx.request;
    await db.collection('locations').insertOne(body);
    ctx.response.body = body;
});

router.get('/location', async ctx => {
    const { username } = ctx.request.query;
    if (username) {
        ctx.response.body = await db.collection('locations').find({ username }).toArray();
    } else {
        ctx.response.body = [];
    }
    });

app.use(router.routes())
    .use(router.allowedMethods());

app.listen(port);

console.log(`Listening on port ${port}.`);
