import { Component, Input, OnInit } from '@angular/core';
import { ILocation } from '../services/locations.service';
import { IWeatherInfo, WeatherService } from '../services/weather.service';

@Component({
  selector: 'app-location-card',
  templateUrl: './location-card.component.html',
  styleUrls: ['./location-card.component.scss']
})
export class LocationCardComponent implements OnInit {

  @Input() location: ILocation | undefined;
  weather: IWeatherInfo | undefined;

  constructor(
    private weatherService: WeatherService
  ) {
  }

  async ngOnInit(): Promise<void> {
    if (this.location) {
      this.weather = await this.weatherService.fetchWeather(this.location.name);
    }
  }

}
