import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  formGroup = this.formBuilder.group({
    username: [null, Validators.required],
    password: [null, Validators.required],
  });

  isLoading = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    if (this.authService.isLogged()) {
      this.router.navigate(['']).then();
    }
  }

  async onLogin(): Promise<void> {
    this.isLoading = true;
    try {
      const { username, password } = this.formGroup.value;
      await this.authService.login(username, password);
      await this.router.navigate(['']);
    } catch (e) {
      alert('Unable to login.');
    } finally {
      this.isLoading = false;
    }
  }
}

