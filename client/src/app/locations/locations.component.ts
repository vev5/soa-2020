import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { ILocation, LocationsService } from '../services/locations.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit, OnDestroy {

  locations: ILocation[] = [];
  subscription: any = null;

  constructor(
    private authService: AuthService,
    private locationsService: LocationsService,
    private router: Router
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.locations = await this.locationsService.fetchLocations();
    this.subscription = this.locationsService.newLocationSubject.subscribe(location => this.locations.push(location));
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async onLogout(): Promise<void> {
    this.authService.logout();
    await this.router.navigate(['login']);
  }

  async onAddLocation(): Promise<void> {
    let location = prompt('Enter the name of a new location');
    if (location && location.trim) {
      location = location.trim();
    }
    if (location) {
      await this.locationsService.addLocation(location);
    }
  }
}
