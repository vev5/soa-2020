import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { webSocket } from 'rxjs/webSocket';
import { Observable, Subject } from 'rxjs';

export interface ILocation {
  _id: string;
  name: string;
  username: string;
}

@Injectable({
  providedIn: 'root'
})
export class LocationsService {
  private apiUrl = environment.apiUrl;
  private newLocation$ = new Subject<ILocation>();

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    const subject = webSocket('ws://localhost:5050');
    subject.next({ username: this.authService.loggedUser?.username });
    subject.subscribe(
      msg => this.newLocation$.next(msg as ILocation),
      err => console.log(err),
      () => this.newLocation$.complete()
    );
  }

  fetchLocations(): Promise<ILocation[]> {
    return this.http.get<ILocation[]>(`${this.apiUrl}/location?username=${this.authService.loggedUser?.username}`, {
      headers: {
        Authorization: `Bearer ${this.authService.token}`
      }
    }).toPromise();
  }

  addLocation(name: string): Promise<ILocation> {
    return this.http.post<ILocation>(`${this.apiUrl}/location`,
      { name, username: this.authService.loggedUser?.username }, {
        headers: {
          Authorization: `Bearer ${this.authService.token}`
        }
      }).toPromise();
  }

  get newLocationSubject(): Observable<ILocation> {
    return this.newLocation$.asObservable();
  }

}
