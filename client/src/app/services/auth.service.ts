import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

export interface IUser {
  username: string;
  password: string;
  token?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl = environment.apiUrl;
  private user: IUser | undefined;

  user$ = new Subject();

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    const user = localStorage.getItem('user');
    if (user) {
      this.user = JSON.parse(user);
      this.user$.next(this.user);
    }
  }

  async login(username: string, password: string): Promise<IUser> {
    return this.http.post<IUser>(`${this.apiUrl}/login`, { username, password }).pipe(
      tap((res: IUser) => {
        if (res.token) {
          localStorage.setItem('user', JSON.stringify(res));
          this.user = res;
          this.user$.next(this.user);
        }
      })
    ).toPromise();
  }

  isLogged(): boolean {
    return !!this.user;
  }

  logout(): void {
    localStorage.removeItem('user');
    this.user = undefined;
    this.user$.next(this.user);
  }

  get token(): string {
    return this.user?.token || '';
  }

  get loggedUser(): IUser | undefined {
    return this.user;
  }
}
