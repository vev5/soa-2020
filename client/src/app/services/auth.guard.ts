import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {
  }

  async canActivate(): Promise<boolean> {
    if (!this.auth.isLogged()) {
      await this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
