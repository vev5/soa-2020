import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';

export interface IWeatherInfo {
  main: {
    temp: number,
    feels_like: number,
    temp_min: number,
    temp_max: number,
    pressure: number,
    humidity: number,
  };
}

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
  }

  fetchWeather(name: string): Promise<IWeatherInfo> {
    return this.http.get<IWeatherInfo>(`${this.apiUrl}/weather?name=${name}`, {
      headers: {
        Authorization: `Bearer ${this.authService.token}`
      }
    }).toPromise();
  }
}
